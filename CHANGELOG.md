# Changelog

## 0.2.3

- Added fixes to allow compilation from Windows. Lots of extra environment variables needed. Look at Readme for more details.

## 0.2.2

- Returned license manager response as bool

## 0.2.1

- Added a check for a .env file in the directory of an overall cargo directory i.e. if included within another package

## 0.2.0

- Changed using raw_struct of face data from the tracker to a wrapped struct
- Added drawing of face data based on Samples VisageDrawing source code
