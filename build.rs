use std::io::{Read, Write};
use dotenv::dotenv;

use std::path::Path;

fn main() {
	dotenv().ok();

	let manifest_dir = build_tools::find_cargo_target_manifest_dir();
	let manifest_env = manifest_dir.join(".env");

	if manifest_env.exists() {
		eprintln!("Manifest Env = {:?}", manifest_env.as_os_str());
		dotenv::from_path(manifest_env).unwrap();
	}

	let target_dir = build_tools::find_cargo_target_dir();

	// NOTE (JB) Handle the c++ wrapper compilation
	let wrapper_dir = "visage_cpp_interface";

	let visage_base_dir = std::env::var("VISAGE_BASE_DIR").expect("Cannot find VISAGE_BASE_DIR in environment. Consider adding to .env file");
	let visage_bin_dir = std::env::var("VISAGE_BIN_DIR").expect("Cannot find VISAGE_BIN_DIR in environment. Consider adding to .env file");
	let visage_lib_dir = std::env::var("VISAGE_LIB_DIR")
		.expect("Cannot find VISAGE_LIB_DIR in environment. Consider adding to .env file");
	let visage_include_dir = std::env::var("VISAGE_INCLUDE_DIR")
		.expect("Cannot find VISAGE_INCLUDE_DIR in environment. Consider adding to .env file");


	eprintln!("Starting Cmake Build");
	let dst = cmake::Config::new(wrapper_dir)
		.target("visage_cpp_interface")
		.define("VISAGE_BASE_DIR", visage_base_dir.clone())
		.build();
	if !cfg!(windows) {
		println!("cargo:rustc-link-search={}/lib", dst.display());
	} else {
		println!("cargo:rustc-link-search={}\\lib", dst.display());
	}
	println!("cargo:rustc-link-lib=visage_cpp_interface");
	eprintln!("Finished CMake Build");


	if !cfg!(windows) {
		copy_files_from_dir_to_target(&visage_lib_dir, &target_dir, "so");
	} else {
		copy_files_from_dir_to_target(&visage_bin_dir, &target_dir, "dll");
	}


	// NOTE (JB) Copy data to test_resources
	let data_dir = Path::new(&visage_base_dir).join("Samples").join("data");
	let data_out_dir = build_tools::find_cargo_target_dir().join("data");
	let test_dir = Path::new("test_resources").join("data");

	let mut copy_opts = fs_extra::dir::CopyOptions::default();
	copy_opts.overwrite = true;
	copy_opts.copy_inside = true;

	let data_dirs = [data_dir];
	fs_extra::copy_items(&data_dirs, data_out_dir, &copy_opts).unwrap();
	fs_extra::copy_items(&data_dirs, test_dir, &copy_opts).unwrap();

	// NOTE (JB) Visage Link Dir
	println!("cargo:rustc-link-search={}", visage_lib_dir);

	// NOTE (JB) Export Libraries to link to
	let visage_libs = if !cfg!(windows) {
		println!("cargo:rustc-link-lib=dylib=stdc++");
		vec![
			"VisageVision",
			"VisageGaze",
			"VisageAnalyser",
			"pthread",
			"openblas",
			"z",
			"OVPlugin",
		]
	} else {
		vec![
			"libVisageVision64",
			"libVisageGaze64",
			"libVisageAnalyser64",
		]
	};

	for v in &visage_libs {
		println!("cargo:rustc-link-lib={}", v);
	}

	// NOTE (JB) Generate Bindings
	println!("cargo:rerun-if-changed=wrapper.h");


	let mut bindings = if !cfg!(windows) {
		// NOTE (JB) OpenCV Linking
		let opencv = pkg_config::probe_library("opencv4").expect("Requires OpenCV in pkgconfig");
		eprintln!("Finished Probe");

		let opencv_clang_args = opencv
			.include_paths
			.iter()
			.map(|p| format!("-I{}", p.display()))
			.collect::<Vec<_>>();

		let mut bindings = bindgen::Builder::default()
			.clang_arg(format!("-I{}", visage_include_dir))
			.clang_arg(format!("-I{}", "/usr/include/c++/9/"))
			.clang_arg(format!("-I{}", "/usr/include/x86_64-linux-gnu/c++/9/"))
			.clang_args(opencv_clang_args)
			.clang_arg("-fdeclspec")
			.clang_arg("-DVISAGE_STATIC")
			.enable_cxx_namespaces()
			.header("wrapper.hpp");

		bindings
	} else {
		let link_dirs = std::env::var("OPENCV_LINK_PATHS").expect("Need OPENCV_LINK_PATHS on Windows").split(",").map(|s| format!("-L{}",s.to_string())).collect::<Vec<_>>();
		let link_libs = std::env::var("OPENCV_LINK_LIBS").expect("Need OPENCV_LINK_LIBS on Windows").split(",").map(|s| format!("-l{}",s.to_string())).collect::<Vec<_>>();
		let include_paths = std::env::var("OPENCV_INCLUDE_PATHS").expect("Need OPENCV_LINK_PATHS on Windows").split(",").map(|s| format!("-I{}",s.to_string())).collect::<Vec<_>>();
		let dll_path_str = std::env::var("OPENCV_DLL_DIR").expect("Need OPENCV_DLL_DIR on Windows");

		copy_files_from_dir_to_target(&dll_path_str, &target_dir, "dll");

		let mut bindings = bindgen::Builder::default()
			.clang_arg(format!("-I{}", visage_include_dir))
			.clang_args(link_dirs.iter())
			.clang_args(link_libs.iter())
			.clang_args(include_paths.iter())
			//.clang_arg("-fdeclspec")
			.clang_arg("-DVISAGE_STATIC")
			.enable_cxx_namespaces()
			.header("wrapper.hpp");

		bindings
	};


	// NOTE (JB) Interface Whitelists
	let int_white_types = ["Tracker"].iter().map(|t| format!("VisageInterface::{}", t));
	let int_white_funcs = ["GetIplImage", "ShowImage", "CompareImage"]
		.iter()
		.map(|t| format!("VisageInterface::{}", t));

	// NOTE (JB) Visage Whitelists
	let vis_white_funcs = ["initializeLicenseManager"].iter().map(|v| format!("VisageSDK::{}", v));

	for t in int_white_types {
		bindings = bindings.allowlist_type(t);
	}

	for f in int_white_funcs.chain(vis_white_funcs) {
		bindings = bindings.allowlist_function(f);
	}

	// included header files changed.
	let bindings = bindings
		.parse_callbacks(Box::new(bindgen::CargoCallbacks))
		// Finish the builder and generate the bindings.
		.generate()
		// Unwrap the Result and panic on failure.
		.expect("Unable to generate bindings");

	// Write the bindings to the srd/bindings.rs file.
	bindings
		.write_to_file("src/bindings.rs")
		.expect("Couldn't write bindings!");

	if cfg!(windows) {
		let mut bindings_text = String::new();
		std::fs::File::open("src/bindings.rs").unwrap().read_to_string(&mut bindings_text).unwrap();

		let bindings_text = bindings_text.replace("??_DTracker@VisageInterface@@QEAAXXZ", "??1Tracker@VisageInterface@@QEAA@XZ");
		std::fs::File::create("src/bindings.rs").unwrap().write_all(bindings_text.as_bytes()).unwrap();
	}
}

fn copy_files_from_dir_to_target(dir: &str, target_dir: &std::path::PathBuf, extension: &str) {
	let found_files = std::fs::read_dir(&dir)
		.unwrap()
		.filter_map(|p| match p {
			Ok(entry) => {
				if entry.path().is_file() {
					if entry.path().extension().unwrap().to_str().unwrap() == extension {
						Some(entry.path())
					} else {
						None
					}
				} else {
					None
				}
			}
			Err(_) => None,
		})
		.collect::<Vec<_>>();

	found_files.iter().for_each(|p| {
		eprintln!("{}", p.display());
		std::fs::copy(p, target_dir.join(p.file_name().unwrap())).unwrap();
	});
}
