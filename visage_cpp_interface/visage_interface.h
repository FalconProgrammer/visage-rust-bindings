#pragma once

#include <VisageFaceRecognition.h>
#include <visageVision.h>
#include <vector>
#include <tuple>

namespace VisageInterface {

    struct TrackerResults {
        int* trackStatus;
        VisageSDK::FaceData* faceData;
        size_t Size;

        TrackerResults(size_t Size);
        ~TrackerResults();
    };

    class Tracker {
    public:
        Tracker(const char * config_file, int maxFaces = 1);
        ~Tracker();

        TrackerResults track(const uint8_t* image_data, int width, int height, int channels, int format = VISAGE_FRAMEGRABBER_FMT_RGB, int origin = VISAGE_FRAMEGRABBER_ORIGIN_TL, long timeStamp = -1);


        VisageSDK::VisageTracker* GetPointer();

    protected:
        VisageSDK::VisageTracker* tracker;
        VisageSDK::FaceData* faceData;
        int numFaces;
    };
}
