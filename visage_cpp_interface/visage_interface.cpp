#include "visage_interface.h"
//#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>

using namespace VisageInterface;
using namespace VisageSDK;


Tracker::Tracker(const char * config_file, int maxFaces) {
    tracker = new VisageTracker(config_file);
    numFaces = maxFaces;
    faceData = new FaceData[maxFaces];
}

Tracker::~Tracker() {
    delete tracker;
    delete [] faceData;
}

VisageSDK::VisageTracker *Tracker::GetPointer() {
    return tracker;
}

TrackerResults
Tracker::track(const uint8_t* image_data, int width, int height, int channels, int format,
               int origin, long timeStamp) {

    TrackerResults res(numFaces);

    int* track_status = tracker->track(width, height, (const char *) image_data, faceData, format, VISAGE_FRAMEGRABBER_ORIGIN_TL, width * channels, timeStamp, numFaces);

    res.trackStatus = track_status;
    res.faceData = faceData;

    return res;
}

TrackerResults::~TrackerResults() {
}

TrackerResults::TrackerResults(size_t Size) {
}
