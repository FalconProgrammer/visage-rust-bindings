use crate::bindings::root::VisageSDK;
use crate::visage_tracker::FaceData;
use image::{DynamicImage, GenericImageView};

#[derive(Default, Copy, Clone)]
pub struct Vec2D {
	pub x: f32,
	pub y: f32,
}

impl Vec2D {
	pub fn new(x: f32, y: f32) -> Vec2D {
		Vec2D { x, y }
	}

	pub fn squared(&self, other: &Self) -> f32 {
		let dx = self.x - other.x;
		let dy = self.y - other.y;

		dx * dx + dy * dy
	}
}

#[derive(Default, Copy, Clone)]
pub struct CubicPoly {
	pub c0: f32,
	pub c1: f32,
	pub c2: f32,
	pub c3: f32,
}

impl CubicPoly {
	pub fn new(c0: f32, c1: f32, c2: f32, c3: f32) -> CubicPoly {
		CubicPoly { c0, c1, c2, c3 }
	}

	pub fn init(x0: f32, x1: f32, t0: f32, t1: f32) -> CubicPoly {
		CubicPoly {
			c0: x0,
			c1: t0,
			c2: -3. * x0 + 3. * x1 - 2. * t0 - t1,
			c3: 2. * x0 - 2. * x1 + t0 + t1,
		}
	}

	pub fn eval(&self, t: f32) -> f32 {
		let t2 = t * t;
		let t3 = t2 * t;

		self.c0 + self.c1 * t + self.c2 * t2 + self.c3 * t3
	}
}

pub fn draw_results(img: &mut DynamicImage, td: &FaceData, draw_only_2d_features: bool) {
	unsafe {
		let contour_points = vec![
			13, 1, 13, 3, 13, 5, 13, 7, 13, 9, 13, 11, 13, 13, 13, 15, 13, 17, 13, 16, 13, 14, 13, 12, 13, 10, 13, 8,
			13, 6, 13, 4, 13, 2,
		];

		let td_raw: &*mut VisageSDK::FaceData = td.raw_struct();
		draw_spline_2d(img, &contour_points, (**td_raw).featurePoints2D);
		draw_points_2d(img, &contour_points, (**td_raw).featurePoints2D, false, *td_raw);

		let outer_upper_lip_points = vec![8, 4, 8, 6, 8, 9, 8, 1, 8, 10, 8, 5, 8, 3];

		draw_spline_2d(img, &outer_upper_lip_points, (**td_raw).featurePoints2D);

		let outer_lower_lip_points = vec![8, 4, 8, 8, 8, 2, 8, 7, 8, 3];

		draw_spline_2d(img, &outer_lower_lip_points, (**td_raw).featurePoints2D);

		let inner_upper_lip_points = vec![2, 5, 2, 7, 2, 2, 2, 6, 2, 4];

		draw_spline_2d(img, &inner_upper_lip_points, (**td_raw).featurePoints2D);

		let inner_lower_lip_points = vec![2, 5, 2, 9, 2, 3, 2, 8, 2, 4];

		draw_spline_2d(img, &inner_lower_lip_points, (**td_raw).featurePoints2D);

		let inner_lip_points = vec![2, 2, 2, 6, 2, 4, 2, 8, 2, 3, 2, 9, 2, 5, 2, 7];

		draw_points_2d(img, &inner_lip_points, (**td_raw).featurePoints2D, false, *td_raw);

		let outer_lip_points = vec![8, 1, 8, 10, 8, 5, 8, 3, 8, 7, 8, 2, 8, 8, 8, 4, 8, 6, 8, 9];

		draw_points_2d(img, &outer_lip_points, (**td_raw).featurePoints2D, false, *td_raw);

		let nose_line_points = vec![9, 1, 9, 3, 9, 2];

		draw_spline_2d(img, &nose_line_points, (**td_raw).featurePoints2D);

		let nose_line_points_2 = vec![9, 3, 14, 22, 14, 23, 14, 24, 14, 25];

		draw_spline_2d(img, &nose_line_points_2, (**td_raw).featurePoints2D);

		let nose_points = vec![9, 1, 9, 2, 9, 3, 9, 15, 14, 22, 14, 23, 14, 24, 14, 25];

		draw_points_2d(img, &nose_points, (**td_raw).featurePoints2D, false, *td_raw);

		if (**td_raw).eyeClosure[0] > 0.5 {
			let iris_points = vec![3, 5];

			draw_points_2d(img, &iris_points, (**td_raw).featurePoints2D, false, *td_raw);
		}

		if (**td_raw).eyeClosure[1] > 0.5 {
			let iris_points = vec![3, 6];

			draw_points_2d(img, &iris_points, (**td_raw).featurePoints2D, false, *td_raw);
		}

		let outer_upper_eye_points_r = vec![3, 12, 3, 14, 3, 8];

		draw_spline_2d(img, &outer_upper_eye_points_r, (**td_raw).featurePoints2D);

		let outer_lower_eye_points_r = vec![3, 8, 3, 10, 3, 12];

		draw_spline_2d(img, &outer_lower_eye_points_r, (**td_raw).featurePoints2D);

		let inner_upper_eye_points_r = vec![3, 12, 12, 10, 3, 2, 12, 6, 3, 8];

		draw_spline_2d(img, &inner_upper_eye_points_r, (**td_raw).featurePoints2D);

		let inner_lower_eye_points_r = vec![3, 8, 12, 8, 3, 4, 12, 12, 3, 12];

		draw_spline_2d(img, &inner_lower_eye_points_r, (**td_raw).featurePoints2D);

		let outer_upper_eye_points_l = vec![3, 11, 3, 13, 3, 7];

		draw_spline_2d(img, &outer_upper_eye_points_l, (**td_raw).featurePoints2D);

		let outer_lower_eye_points_l = vec![3, 7, 3, 9, 3, 11];

		draw_spline_2d(img, &outer_lower_eye_points_l, (**td_raw).featurePoints2D);

		let inner_upper_eye_points_l = vec![3, 11, 12, 9, 3, 1, 12, 5, 3, 7];

		draw_spline_2d(img, &inner_upper_eye_points_l, (**td_raw).featurePoints2D);

		let eyes_points = vec![3, 2, 3, 4, 3, 8, 3, 10, 3, 12, 3, 14, 12, 6, 12, 8, 12, 10, 12, 12];

		let eyes_points_2 = vec![3, 1, 3, 3, 3, 7, 3, 9, 3, 11, 3, 13, 12, 5, 12, 7, 12, 9, 12, 11];

		draw_points_2d(
			img,
			&eyes_points,
			(**td_raw).featurePoints2D,
			(**td_raw).eyeClosure[1] < 0.5,
			*td_raw,
		);
		draw_points_2d(
			img,
			&eyes_points_2,
			(**td_raw).featurePoints2D,
			(**td_raw).eyeClosure[0] < 0.5,
			*td_raw,
		);

		let eyebrow_lines_r = vec![4, 6, 14, 4, 4, 4, 14, 2, 4, 2];

		draw_spline_2d(img, &eyebrow_lines_r, (**td_raw).featurePoints2D);

		let eyebrow_lines_l = vec![4, 1, 14, 1, 4, 3, 14, 3, 4, 5];

		draw_spline_2d(img, &eyebrow_lines_l, (**td_raw).featurePoints2D);

		let eyebrow_points = vec![4, 1, 4, 2, 4, 3, 4, 4, 4, 5, 4, 6, 14, 1, 14, 2, 14, 3, 14, 4];

		draw_points_2d(img, &eyebrow_points, (**td_raw).featurePoints2D, false, *td_raw);

		if !draw_only_2d_features {
			draw_face_model_axes(img, *td_raw);
		}

		let leye = (*(**td_raw).featurePoints2D).getFP(3, 5);
		let reye = (*(**td_raw).featurePoints2D).getFP(3, 6);

		let l_rad = (**td_raw).irisRadius[0];
		let r_rad = (**td_raw).irisRadius[1];

		let colour = image::Rgba::from([255_u8, 255, 255, 255]);

		if l_rad > 0. {
			imageproc::drawing::draw_hollow_circle_mut(
				img,
				(
					((*leye).pos[0] * img.width() as f32).round() as i32,
					((1.0 - (*leye).pos[1]) * img.height() as f32).round() as i32,
				),
				(**td_raw).irisRadius[0].round() as i32,
				colour,
			);
		}

		if r_rad > 0. {
			imageproc::drawing::draw_hollow_circle_mut(
				img,
				(
					((*reye).pos[0] * img.width() as f32).round() as i32,
					((1.0 - (*reye).pos[1]) * img.height() as f32).round() as i32,
				),
				(**td_raw).irisRadius[1].round() as i32,
				colour,
			);
		}
	}
}

unsafe fn draw_spline_2d(img: &mut DynamicImage, points: &[i32], fp2d: *mut VisageSDK::FDP) {
	let num = points.len() / 2;

	if num < 2 {
		return;
	}

	let mut point_coords = Vec::<f32>::new();
	let mut n = 0;

	for i in 0..num {
		let fp = (*fp2d).getFP(points[2 * i], points[2 * i + 1]);

		if (*fp).defined != 0 {
			if (*fp).pos[0] != 0. && (*fp).pos[1] != 0. {
				point_coords.push((*fp).pos[0]);
				point_coords.push((*fp).pos[1]);
				n += 1
			}
		}
	}

	if point_coords.is_empty() || n <= 2 {
		return;
	}

	let factor = 10;
	let points_to_draw = calc_spline(&mut point_coords, factor);
	let n_vert = points_to_draw.len() / 2;

	let mut vertices = vec![0_f32; n_vert * 2];
	let mut counter = 0;

	for i in 0..n_vert {
		vertices[2 * i + 0] = points_to_draw[counter] * img.width() as f32;
		counter += 1;

		vertices[2 * i + 1] = (1. - points_to_draw[counter]) * img.height() as f32;
		counter += 1;
	}

	let colour = image::Rgba::from([176, 196, 222_u8, 160]);

	for j in 0..n_vert - 1 {

		let vert1 = (vertices[2 * j], vertices[2 * j + 1]);
		let vert2 = (vertices[2 * (j + 1)], vertices[2 * (j + 1) + 1]);

		imageproc::drawing::draw_line_segment_mut(
			img,
			vert1,
			vert2,
			colour,
		);
	}
}

fn calc_spline(input_points: &mut Vec<f32>, ratio: usize) -> Vec<f32> {
	let n_points = input_points.len() / 2 + 2;
	let n_points_to_draw = input_points.len() / 2 + (input_points.len() / 2 - 1) * ratio;
	let n_lines = n_points - 1 - 2;

	// Adding fake points co-ordinates
	input_points.insert(0, input_points[1] + (input_points[1] - input_points[3]));
	input_points.insert(0, input_points[1] + (input_points[1] - input_points[3]));
	input_points.insert(
		input_points.len(),
		input_points[input_points.len() / 2 - 2]
			+ (input_points[input_points.len() / 2 - 2] - input_points[input_points.len() / 2 - 4]),
	);
	input_points.insert(
		input_points.len(),
		input_points[input_points.len() / 2 - 1]
			+ (input_points[input_points.len() / 2 - 1] - input_points[input_points.len() / 2 - 3]),
	);

	//  calculate spline

	let mut output_points = vec![0_f32; 2 * n_points_to_draw];

	for i in 0..n_points - 2 {
		output_points[i * 2 * (ratio + 1)] = input_points[2 * i + 2];
		output_points[i * 2 * (ratio + 1) + 1] = input_points[2 * i + 1 + 2];
	}

	for i in (0..2 * n_lines).step_by(2) {
		let p0 = Vec2D::new(input_points[i], input_points[i + 1]);
		let p1 = Vec2D::new(input_points[i + 2], input_points[i + 3]);
		let p2 = Vec2D::new(input_points[i + 4], input_points[i + 5]);
		let p3 = Vec2D::new(input_points[i + 6], input_points[i + 7]);

		let (px, py) = init_centripetal_cr(&p0, &p1, &p2, &p3);

		for j in 1..ratio+1 {
			let tpx = px.eval(1_f32 / ((ratio + 1) * j) as f32);
			let tpy = py.eval(1_f32 / ((ratio + 1) * j) as f32);
			output_points[i * (ratio + 1) + 2 * j] = tpx;
			output_points[i * (ratio + 1) + 2 * j + 1] = tpy;
		}
	}

	for i in (0..2).rev() {
		input_points.remove(i);
	}

	input_points.remove(input_points.len()-2);
	input_points.remove(input_points.len()-1);

	output_points
}

fn init_centripetal_cr(p0: &Vec2D, p1: &Vec2D, p2: &Vec2D, p3: &Vec2D) -> (CubicPoly, CubicPoly) {
	let mut dt0 = p0.squared(p1).powf(0.25);
	let mut dt1 = p1.squared(p2).powf(0.25);
	let mut dt2 = p2.squared(p3).powf(0.25);

	if dt1 < 1e-4 {
		dt1 = 1.0;
	}

	if dt0 < 1e-4 {
		dt0 = dt1;
	}

	if dt2 < 1e-4 {
		dt2 = dt1;
	}

	(
		init_non_uniform_catmull_rom(p0.x, p1.x, p2.x, p3.x, dt0, dt1, dt2),
		init_non_uniform_catmull_rom(p0.y, p1.y, p2.y, p3.y, dt0, dt1, dt1),
	)
}

fn init_non_uniform_catmull_rom(x0: f32, x1: f32, x2: f32, x3: f32, dt0: f32, dt1: f32, dt2: f32) -> CubicPoly {
	// compute tangents when parameterized in [t1,t2]
	let mut t1 = (x1 - x0) / dt0 - (x2 - x0) / (dt0 + dt1) + (x2 - x1) / dt1;
	let mut t2 = (x2 - x1) / dt1 - (x3 - x1) / (dt1 + dt2) + (x3 - x2) / dt2;

	// rescale tangents for parameterization in [0,1]
	t1 *= dt1;
	t2 *= dt2;

	CubicPoly::init(x1, x2, t1, t2)
}

#[cfg(test)]
mod tests {
	use crate::visage_drawing::CubicPoly;

	#[test]
	fn cubic_poly_init() {
		let point = CubicPoly::init(10., 10., 10., 10.);

		assert_eq!(point.c0, 10.);
		assert_eq!(point.c1, 10.);
		assert_eq!(point.c2, -30.);
		assert_eq!(point.c3, 20.);
	}
}

unsafe fn draw_points_2d(
	img: &mut DynamicImage,
	points: &[i32],
	fp2d: *mut VisageSDK::FDP,
	single_colour: bool,
	td: *mut VisageSDK::FaceData,
) {
	let num = points.len() / 2;
	let radius = ((*td).faceScale as f32 * 0.02_f32).max(2.);

	for i in 0..num {
		let fp = (*fp2d).getFP(points[2 * i], points[2 * i + 1]);

		if (*fp).defined != 0 {
			if (*fp).pos[0] != 0. && (*fp).pos[1] != 0. {
				let x = (*fp).pos[0] * img.width() as f32;
				let y = (1. - (*fp).pos[1]) * img.height() as f32;

				let colour = image::Rgba::from([0_u8, 0, 0, 255]);
				imageproc::drawing::draw_filled_circle_mut(img, (x as i32, y as i32), radius.round() as i32, colour);

				if !single_colour {
					let radius2 = (radius * 0.6).round().min(radius - 1.);
					let colour = image::Rgba::from([0_u8, 255, 255, 255]);
					imageproc::drawing::draw_filled_circle_mut(
						img,
						(x as i32, y as i32),
						radius2.round() as i32,
						colour,
					);
				}
			}
		}
	}
}

unsafe fn draw_face_model_axes(img: &mut DynamicImage, tracking_data: *mut VisageSDK::FaceData) {
	//precalculate sines and cosines for eye rotations
	let r = (*tracking_data).faceRotation;
	let f = (*tracking_data).cameraFocus;

	let fdp = (*tracking_data).featurePoints3D;
	let fp1 = (*fdp).getFP(4, 1);
	let fp2 = (*fdp).getFP(4, 2);

	let t = vec![
		((*fp1).pos[0] + (*fp2).pos[0]) / 2.0,
		((*fp1).pos[1] + (*fp2).pos[1]) / 2.0,
		((*fp1).pos[2] + (*fp2).pos[2]) / 2.0,
	];

	//pre calculate sines and cosines
	let sinrx = r[0].sin();
	let sinry = (r[1] + std::f32::consts::PI).sin();
	let sinrz = r[2].sin();

	let cosrx = r[0].cos();
	let cosry = (r[1] + std::f32::consts::PI).cos();
	let cosrz = r[2].cos();

	//set the rotation matrix
	let r00 = cosry * cosrz + sinrx * sinry * sinrz;
	let r01 = -cosry * sinrz + sinrx * sinry * cosrz;
	let r02 = cosrx * sinry;
	let r10 = cosrx * sinrz;
	let r11 = cosrx * cosrz;
	let r12 = -sinrx;
	let r20 = -sinry * cosrz + sinrx * cosry * sinrz;
	let r21 = sinry * sinrz + sinrx * cosry * cosrz;
	let r22 = cosrx * cosry;

	let vertices = vec![
		0.0_f32, 0.0, 0.0, 0.07, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.07, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.07,
	];

	let mut vertices_2d = vec![0_f32; 2 * 6];

	// variables for aspect correction
	let mut x_offset = 1_f32;
	let mut y_offset = 1_f32;
	let w = img.width() as f32;
	let h = img.height() as f32;

	if w > h {
		x_offset = w / h;
	} else if w < h {
		y_offset = h / w;
	}

	//loop over all vertices
	for i in 0..6 {
		let x0 = vertices[3 * i + 0];
		let y0 = vertices[3 * i + 1];
		let z0 = vertices[3 * i + 2];

		//scale, rotation and translation added
		let x1 = r00 * x0 + r01 * y0 + r02 * z0 + t[0];
		let y1 = r10 * x0 + r11 * y0 + r12 * z0 + t[1];
		let z1 = r20 * x0 + r21 * y0 + r22 * z0 + t[2];

		//projection
		let fdz = f / z1;

		let x = fdz * x1;
		let y = fdz * y1;

		// to screen space
		vertices_2d[2 * i + 0] = ((x * -0.5_f32 / x_offset) + 0.5) * w;
		vertices_2d[2 * i + 1] = (1.0_f32 - ((y * 0.5 / y_offset) + 0.5)) * h;
	}

	let colour_x = image::Rgba::from([255_u8, 0, 0, 255]);
	let colour_y = image::Rgba::from([0_u8, 0, 255, 255]);
	let colour_z = image::Rgba::from([0_u8, 255, 0, 255]);

	imageproc::drawing::draw_line_segment_mut(
		img,
		(vertices_2d[0], vertices_2d[1]),
		(vertices_2d[2], vertices_2d[3]),
		colour_x,
	);
	imageproc::drawing::draw_line_segment_mut(
		img,
		(vertices_2d[4], vertices_2d[5]),
		(vertices_2d[6], vertices_2d[7]),
		colour_y,
	);
	imageproc::drawing::draw_line_segment_mut(
		img,
		(vertices_2d[8], vertices_2d[9]),
		(vertices_2d[10], vertices_2d[11]),
		colour_z,
	);
}
