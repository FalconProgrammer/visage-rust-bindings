#![allow(dead_code)]

#[macro_use]
extern crate strum_macros;

extern crate openmp_sys;

pub mod bindings;

mod visage_functions;
//pub mod visage_face_recognition;
mod cv_image_types;
pub mod visage_drawing;
pub mod visage_tracker;

pub use visage_functions::*;

#[cfg(test)]
mod tests {
	#[test]
	fn it_works() {
		assert_eq!(2 + 2, 4);
	}
}
