// NOTE (JB) Taken from Rust OpenCV Project

const CV_DEPTH_MAX: i32 = 1 << CV_CN_SHIFT;
const CV_MAT_DEPTH_MASK: i32 = CV_DEPTH_MAX - 1;
const CV_CN_SHIFT: i32 = 3;

#[inline(always)]
pub const fn CV_MAT_DEPTH(flags: i32) -> i32 {
	#![allow(non_snake_case)]
	flags & CV_MAT_DEPTH_MASK
}

#[inline(always)]
pub const fn CV_MAKETYPE(depth: i32, cn: i32) -> i32 {
	#![allow(non_snake_case)]
	CV_MAT_DEPTH(depth) + ((cn - 1) << CV_CN_SHIFT)
}

#[allow(non_camel_case_types)]
pub enum CvDepth {
	CV_8U = 0,
	CV_8S = 1,
	CV_16U = 2,
	CV_16S = 3,
	CV_32S = 4,
	CV_32F = 5,
	CV_64F = 6,
	CV_16F = 7,
}

use CvDepth::*;

pub const CV_16FC1: i32 = CV_MAKETYPE(CV_16F as i32, 1);
pub const CV_16FC2: i32 = CV_MAKETYPE(CV_16F as i32, 2);
pub const CV_16FC3: i32 = CV_MAKETYPE(CV_16F as i32, 3);
pub const CV_16FC4: i32 = CV_MAKETYPE(CV_16F as i32, 4);

pub const CV_16SC1: i32 = CV_MAKETYPE(CV_16S as i32, 1);
pub const CV_16SC2: i32 = CV_MAKETYPE(CV_16S as i32, 2);
pub const CV_16SC3: i32 = CV_MAKETYPE(CV_16S as i32, 3);
pub const CV_16SC4: i32 = CV_MAKETYPE(CV_16S as i32, 4);

pub const CV_16UC1: i32 = CV_MAKETYPE(CV_16U as i32, 1);
pub const CV_16UC2: i32 = CV_MAKETYPE(CV_16U as i32, 2);
pub const CV_16UC3: i32 = CV_MAKETYPE(CV_16U as i32, 3);
pub const CV_16UC4: i32 = CV_MAKETYPE(CV_16U as i32, 4);

pub const CV_32FC1: i32 = CV_MAKETYPE(CV_32F as i32, 1);
pub const CV_32FC2: i32 = CV_MAKETYPE(CV_32F as i32, 2);
pub const CV_32FC3: i32 = CV_MAKETYPE(CV_32F as i32, 3);
pub const CV_32FC4: i32 = CV_MAKETYPE(CV_32F as i32, 4);

pub const CV_32SC1: i32 = CV_MAKETYPE(CV_32S as i32, 1);
pub const CV_32SC2: i32 = CV_MAKETYPE(CV_32S as i32, 2);
pub const CV_32SC3: i32 = CV_MAKETYPE(CV_32S as i32, 3);
pub const CV_32SC4: i32 = CV_MAKETYPE(CV_32S as i32, 4);

pub const CV_64FC1: i32 = CV_MAKETYPE(CV_64F as i32, 1);
pub const CV_64FC2: i32 = CV_MAKETYPE(CV_64F as i32, 2);
pub const CV_64FC3: i32 = CV_MAKETYPE(CV_64F as i32, 3);
pub const CV_64FC4: i32 = CV_MAKETYPE(CV_64F as i32, 4);

pub const CV_8SC1: i32 = CV_MAKETYPE(CV_8S as i32, 1);
pub const CV_8SC2: i32 = CV_MAKETYPE(CV_8S as i32, 2);
pub const CV_8SC3: i32 = CV_MAKETYPE(CV_8S as i32, 3);
pub const CV_8SC4: i32 = CV_MAKETYPE(CV_8S as i32, 4);

pub const CV_8UC1: i32 = CV_MAKETYPE(CV_8U as i32, 1);
pub const CV_8UC2: i32 = CV_MAKETYPE(CV_8U as i32, 2);
pub const CV_8UC3: i32 = CV_MAKETYPE(CV_8U as i32, 3);
pub const CV_8UC4: i32 = CV_MAKETYPE(CV_8U as i32, 4);
