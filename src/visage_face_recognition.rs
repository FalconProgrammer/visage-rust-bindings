use crate::bindings::root::VisageSDK;
use std::ffi;

pub struct VisageFaceRecognition {
    raw_struct : VisageSDK::VisageFaceRecognition
}

impl VisageFaceRecognition {
    pub fn new(data_path: &str) -> VisageFaceRecognition {
        let c_data_path = ffi::CString::new(data_path).expect("Unable to convert data_path to c string");

        let raw = unsafe {
             VisageSDK::VisageFaceRecognition::new(c_data_path.as_ptr())
        };

        VisageFaceRecognition { raw_struct : raw }
    }

    pub fn get_raw(&mut self) -> &mut VisageSDK::VisageFaceRecognition {
        return &mut self.raw_struct;
    }
}

// TODO (JB) Destruct causes segfault, needs investigation
/*impl Drop for VisageFaceRecognition {
    fn drop(&mut self) {
        unsafe {
            dbg!("Destructing...");
            self.raw_struct.destruct();
            dbg!("Destructing Complete");
        }
    }
}*/