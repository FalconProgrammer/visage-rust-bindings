use crate::bindings::root::{VisageInterface, VisageSDK};
use getset::*;
use image::GenericImageView;
use std::ffi;

pub struct VisageTracker {
	raw_struct: VisageInterface::Tracker,
	num_faces: i32,
}

#[derive(Getters)]
pub struct TrackerResults {
	#[getset(get = "pub")]
	track_status: Vec<TrackStat>,

	#[getset(get = "pub")]
	face_data: Vec<FaceData>,
}

#[derive(Getters)]
pub struct FaceData {
	#[getset(get = "pub")]
	raw_struct: *mut VisageSDK::FaceData,
}

impl From<*mut VisageSDK::FaceData> for FaceData {
	fn from(ptr: *mut VisageSDK::FaceData) -> Self {
		FaceData { raw_struct: ptr }
	}
}

/*
#define VISAGE_CAMERA_UP 0
#define VISAGE_CAMERA_DOWN 1
#define VISAGE_CAMERA_LEFT 2
#define VISAGE_CAMERA_RIGHT 3
*/
#[derive(Display, Debug, Eq, PartialEq)]
pub enum TrackStat {
	OFF = 0,
	OK = 1,
	RECOVERING = 2,
	INIT = 3,
}

#[derive(Display, Debug)]
pub enum FramegrabberFmt {
	RGB = 0,
	BGR = 1,
	LUMINANCE = 2,
	RGBA = 3,
	BGRA = 4,
}

pub enum FramegrabberOrigin {
	TL = 0,
	BL = 1,
}

impl FramegrabberFmt {
	pub fn from_image(image: &image::DynamicImage) -> Result<FramegrabberFmt, &str> {
		use image::DynamicImage;

		match image {
			DynamicImage::ImageRgb8(_) => Ok(FramegrabberFmt::RGB),
			DynamicImage::ImageBgr8(_) => Ok(FramegrabberFmt::BGR),
			DynamicImage::ImageLuma8(_) => Ok(FramegrabberFmt::LUMINANCE),
			DynamicImage::ImageRgba8(_) => Ok(FramegrabberFmt::RGBA),
			DynamicImage::ImageBgra8(_) => Ok(FramegrabberFmt::BGRA),
			_ => Err("Unrecognised Image Type. Try converting to 8-bit format."),
		}
	}

	pub fn from_buffer<I: image::Pixel>(_image: &image::ImageBuffer<I, Vec<u8>>) -> Result<FramegrabberFmt, &str> {
		use image::ColorType;

		match I::COLOR_TYPE {
			ColorType::Rgb8 => Ok(FramegrabberFmt::RGB),
			ColorType::Bgr8 => Ok(FramegrabberFmt::BGR),
			ColorType::L8 => Ok(FramegrabberFmt::LUMINANCE),
			ColorType::Rgba8 => Ok(FramegrabberFmt::RGBA),
			ColorType::Bgra8 => Ok(FramegrabberFmt::BGRA),
			_ => Err("Unrecognised Image Type. Try converting to 8-bit format."),
		}
	}

	pub fn to_cv_type(&self) -> i32 {
		use super::cv_image_types::*;
		use FramegrabberFmt::*;

		match self {
			RGB => CV_8UC3,
			BGR => CV_8UC3,
			LUMINANCE => CV_8UC1,
			RGBA => CV_8UC4,
			BGRA => CV_8UC4,
		}
	}
}

#[cfg(test)]
mod framegrabber_tests {
	use crate::cv_image_types as cv;
	use crate::visage_tracker::FramegrabberFmt;

	#[test]
	fn to_cv_type() {
		let v = FramegrabberFmt::RGB;
		assert_eq!(v.to_cv_type(), cv::CV_8UC3);

		let v = FramegrabberFmt::BGR;
		assert_eq!(v.to_cv_type(), cv::CV_8UC3);

		let v = FramegrabberFmt::LUMINANCE;
		assert_eq!(v.to_cv_type(), cv::CV_8UC1);

		let v = FramegrabberFmt::RGBA;
		assert_eq!(v.to_cv_type(), cv::CV_8UC4);

		let v = FramegrabberFmt::BGRA;
		assert_eq!(v.to_cv_type(), cv::CV_8UC4);
	}
}

impl VisageTracker {
	pub fn new(settings_path: &str, num_faces: i32) -> VisageTracker {
		let c_data_path = ffi::CString::new(settings_path).expect("Unable to convert data_path to c string");

		let raw = unsafe {
			use std::os::raw::c_int;

			VisageInterface::Tracker::new(c_data_path.as_ptr(), c_int::from(num_faces))
		};

		VisageTracker {
			raw_struct: raw,
			num_faces,
		}
	}

	pub fn track_raw(
		&mut self,
		input_vec: &[u8],
		width: i32,
		height: i32,
		fg_t: FramegrabberFmt,
		origin: FramegrabberOrigin,
		timestamp: i64,
	) -> Result<TrackerResults, &str> {
		let channels = match &fg_t {
			FramegrabberFmt::RGB | FramegrabberFmt::BGR => 3,
			FramegrabberFmt::BGRA | FramegrabberFmt::RGBA => 4,
			FramegrabberFmt::LUMINANCE => 1,
		};

		unsafe {
			use std::os::raw::{c_int, c_long};

			let res = self.raw_struct.track(
				input_vec.as_ptr(),
				width,
				height,
				channels,
				c_int::from(fg_t as i32),
				c_int::from(origin as i32),
				timestamp as c_long,
			);

			let mut tracking_stats = Vec::<TrackStat>::new();
			let mut face_data = Vec::<FaceData>::new();

			for i in 0..self.num_faces {
				let status = match *res.trackStatus.offset(i as isize) {
					0 => TrackStat::OFF,
					1 => TrackStat::OK,
					2 => TrackStat::RECOVERING,
					3 => TrackStat::INIT,
					_ => panic!("Should be Impossible!"),
				};
				tracking_stats.push(status);
				face_data.push(FaceData::from(res.faceData.offset(i as isize)));
			}

			return Ok(TrackerResults {
				track_status: tracking_stats,
				face_data,
			});
		}
	}

	pub fn track(
		&mut self,
		input_image: &image::DynamicImage,
		origin: FramegrabberOrigin,
		timestamp: i64,
	) -> Result<TrackerResults, &str> {
		let fg_t = match FramegrabberFmt::from_image(&input_image) {
			Ok(f) => f,
			Err(_e) => return Err("Unrecognised Image Type. Try converting to 8-bit format."),
		};

		return self.track_raw(
			&input_image.to_bytes(),
			input_image.width() as i32,
			input_image.height() as i32,
			fg_t,
			origin,
			timestamp,
		);
	}

	pub fn get_raw(&mut self) -> &mut VisageInterface::Tracker {
		return &mut self.raw_struct;
	}
}

impl Drop for VisageTracker {
	fn drop(&mut self) {
		unsafe {
			self.raw_struct.destruct();
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn initialize_run_test() {
		dbg!("Opening Image");
		let image = image::open(std::path::PathBuf::from(format!(
			"{}/test_resources/002_white.png",
			env!("CARGO_MANIFEST_DIR")
		)))
		.unwrap();
		println!("Opened. Creating Trakcer");

		let mut tracker = VisageTracker::new(
			&format!(
				"{}/test_resources/data/Facial Features Tracker - High.cfg",
				env!("CARGO_MANIFEST_DIR")
			)
			.to_string(),
			4,
		);
		println!("Created. Trying to track");

		let res = tracker.track(&image, FramegrabberOrigin::TL, 0).unwrap();

		println!("{:?}", res.track_status);
	}
}
