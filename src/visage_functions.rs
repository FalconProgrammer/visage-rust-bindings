use crate::bindings::root::VisageSDK;
use std::ffi;

pub fn initialize_license_manager(path: &str) -> bool {
	let c_path = ffi::CString::new(path).expect("Unable to parse path");
	let r_code = unsafe {
		VisageSDK::initializeLicenseManager(c_path.as_ptr())
	};

	if r_code == 1 {true} else {false}
}

#[cfg(test)]
mod tests {
	#[test]
	fn initialize_test() {
		assert!(!super::initialize_license_manager(""));
	}
}
